# README #

This is an example Thesis template for HDR students in the School of Electrical and Data Engineering at the University of Technology Sydney

### What is this repository for? ###

* This Thesis template satisfies the requirements for PhD and Research Masters students at UTS. It has been set up for SEDE students, but may be of value to others.
* Version: 1.0 (March 2022)

### How do I get set up? ###

* Clone the repository: git clone [https://bitbucket.org/DanielFranklin/latex-thesis-template](https://bitbucket.org/DanielFranklin/latex-thesis-template)
* Enter the latex-thesis-template folder and run: pdflatex thesis.tex ; biber thesis ; pdflatex thesis.tex ; pdflatex thesis.tex
* Dependencies: a modern LaTeX installation including biblatex and all the standard packages

### Useful Resources ###

* For help with things like tables, figures, algorithms, code highlighting etc., you can refer to the [LaTeX Wikibook](https://en.wikibooks.org/wiki/LaTeX)
* For your figures, try to use vector graphics formats wherever possible. A great open source tool for this is [Inkscape](https://www.inkscape.org)
* The suggested workflow is to create the figure and save it (for future modification) in scalable vector graphics (SVG) format, then export to PDF when you are ready to include it in your document.
* If you have raster images, you can also edit these using the [open source tool GIMP](https://www.gimp.org)

### Contribution guidelines ###

* Suggestions, questions or comments? Please forward to a representative of the SEDE School Research Management Committee, or directly contact the maintainer (see below).

### Who do I talk to? ###

* The current maintainer is [Dr Daniel Franklin](mailto:Daniel.Franklin@uts.edu.au)
